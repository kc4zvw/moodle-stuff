# Moodle Stuff

 Miscellaneous notes and other stuff for classes

# VPS server details

Request submitted: 20-September-2022 by DD


Login details:

    David Billsbrough (kc4zvw)

    Matrix: @kc4zvw:matrix.org
    IRC: @kc4zvw
    Salsa: (here)
    Wiki:  https://wiki.debian.org/kc4zvw

 my public key:
   
    ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIa/BhUBJDKPr6NcJN7D5AgB+Qb5uOjcAuurUleIc9pTZyATiuGAsT1CGokAH59ZTzfoJ7SW2OmJXvLNBcmq1wY=   kc4zvw@www.kc4zvw.org




## What is the plan?

* Provision a moodle instance
* Create course outline
* Make intro video
* Make an overall slideshow
* Item5


----
David (KC4ZVW)
